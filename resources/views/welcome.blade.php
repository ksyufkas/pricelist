@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Добро пожаловать!</div>
                @if (Auth::guest())
                <div class="panel-body">
                    Пожалуйста, авторизируйтесь для работы с прайс листом.
                </div>
                @else
                <div class="panel-body">
                    Перейдите в раздел <a href="products/">Список товаров</a> для работы с прайс листом.
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
