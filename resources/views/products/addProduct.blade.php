<!-- resources/views/products/addProduct.blade.php -->

@extends('layouts.app')

@section('content')

<div class="container">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Создание продукта
            </div>
            <div class="panel-body">
                @include('common.errors')

                <form action="{{ url('products/add-product/add') }}" method="POST" class="form-horizontal">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="product-name" class="col-sm-3 control-label">Название</label>

                        <div class="col-sm-6">
                            <input type="text" name="name" id="product-name" class="form-control" require>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="product-unit" class="col-sm-3 control-label">Единицы измерения</label>
                        <div class="col-sm-6">
                            <select
                                class="form-control"
                                name="unit_id"
                                id="product-unit"
                            >
                                <option value="0" selected>Не выбрано</option>
                            @foreach ($units as $unit)
                                <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="product-price" class="col-sm-3 control-label">Цена</label>

                        <div class="col-sm-6">
                            <input type="number" step="0.01" min="0.01" name="price" id="product-price"
                                class="form-control" require>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="product-count" class="col-sm-3 control-label">Количество</label>

                        <div class="col-sm-6">
                            <input type="number" step="0.01" min="0.01" name="count" id="product-count"
                                class="form-control" require>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-plus"></i> Добавить продукт
                            </button>
                            <button type="button" class="btn btn-danger" onclick="window.location='/products';">
                                <i class="fa fa-back"></i> Отмена
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection