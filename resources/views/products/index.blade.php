<!-- resources/views/products/index.blade.php -->

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-sm-12">
        @if (count($products) > 0)
        <div class="panel panel-default">
            <div class="panel-heading">
                Просмотр прайс листа
            </div>

            <div class="panel-body">
                <table class="table table-striped">
                    <thead>
                        <th>Прайс лист</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="table-text">
                                <div>Название</div>
                            </td>
                            <td class="table-text">
                                <div>Единицы измерения</div>
                            </td>
                            <td class="table-text">
                                <div>Цена</div>
                            </td>
                            <td class="table-text">
                                <div>Количество</div>
                            </td>
                            <td class="table-text">
                                <div>Редактировать</div>
                            </td>
                            <td class="table-text">
                                <div>Удалить</div>
                            </td>
                        </tr>
                        @foreach ($products as $product)
                        <tr>
                            <td class="table-text">
                                <div>{{ $product->name }}</div>
                            </td>
                            <td class="table-text">
                                <div>{{ $product->unit->name }}</div>
                            </td>
                            <td class="table-number">
                                <div>{{ $product->price }}</div>
                            </td>
                            <td class="table-number">
                                <div>{{ $product->count }}</div>
                            </td>

                            <td>
                                <button
                                    type="button"
                                    class="btn btn-warning"
                                    onclick="window.location='/products/edit-product/{{$product->id}}'"
                                >
                                    <i class="fa fa-edit"></i> Edit
                                </button>
                            </td>
                            <td>
                                <form
                                    action="{{ url('products/delete-product/'.$product->id) }}"
                                    method="POST"
                                >
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <button
                                        type="submit"
                                        class="btn btn-danger"
                                    >
                                        <i class="fa fa-btn fa-trash"></i>Delete
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endif
        <div class="col-sm-offset-3 col-sm-6">
            <button
                type="button"
                class="btn btn-primary"
                onclick="window.location='products/add-product';"
            >
                <i class="fa fa-plus"></i> Добавить продукт
            </button>
        </div>
    </div>
</div>
@endsection