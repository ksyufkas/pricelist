<?php

use Illuminate\Database\Seeder;
use App\Unit;

class UnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Unit::create([ 'name' => 'кг.']);
        Unit::create([ 'name' => 'г.']);
        Unit::create([ 'name' => 'шт.']);
        Unit::create([ 'name' => 'уп.']);
        Unit::create([ 'name' => 'л.']);
        Unit::create([ 'name' => 'мл.']);
        Unit::create([ 'name' => 'м2.']);
        Unit::create([ 'name' => 'м3.']);
        Unit::create([ 'name' => 'см.']);
        Unit::create([ 'name' => 'мм.']);
        Unit::create([ 'name' => 'бут.']);
    }
}
