<?php

namespace App;

use App\User;

use App\Unit;

use Illuminate\Database\Eloquent\Model;

class Product extends Model 
{
    protected $fillable = ['name', 'price', 'count', 'unit_id'];

    public function user() 
    {
        return $this->belongsTo(User::class);
    }

    public function unit() 
    {
        return $this->belongsTo(Unit::class);
    }
}
