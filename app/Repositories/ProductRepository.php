<?php

namespace App\Repositories;

use App\User;
use App\Product;

class ProductRepository
{
  public function forUser(User $user)
  {
    return $user->products()
              ->orderBy('created_at', 'asc')
              ->get();
  }
}