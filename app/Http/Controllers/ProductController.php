<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Repositories\ProductRepository;

use DB;

use App\Product;

use App\Unit;

class ProductController extends Controller
{
    protected $products;
    protected $units;

    public function __construct(ProductRepository $products)
    {
        $this->middleware('auth');
        $this->products = $products;
    }
  
    public function index(Request $request)
    {
        return view('products.index', [
            'products' => $this->products->forUser($request->user()),
        ]);
    }
  
    public function addProduct(Request $request)
    {
        $units = DB::table('units')->get();
        return view('products.addProduct', [
            'units' => $units,
        ]);
    }
  
    public function editProduct(Request $request, Product $product)
    {
        $units = DB::table('units')->get();

        return view('products.editProduct', [
            'product' => $product,
            'units' => $units,
        ]);
    }
  
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'unit_id' => 'required|numeric|min:1',
            'price' => 'required|numeric|min:0.1',
            'count' => 'required|numeric|min:0.1',
        ]);

        $product = new Product(array(
            'name' => $request->name,
            'unit_id' => $request->unit_id,
            'price' => $request->price,
            'count' => $request->count,
        ));

        $request->user()->products()->save($product);
        
        return redirect('/products');
    }
  
    public function updateProduct(Request $request, Product $product)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'unit_id' => 'required|numeric|min:1',
            'price' => 'required|numeric|min:0.1',
            'count' => 'required|numeric|min:0.1',
        ]);

        $request->user()->products()->where('id', $product->id)->update([
            'name' => $request->name,
            'unit_id' => $request->unit_id,
            'price' => $request->price,
            'count' => $request->count,
        ]);
        
        return redirect('/products');
    }

    public function destroy(Request $request, Product $product)
    {
        $this->authorize('destroy', $product);
        $product->delete();

        return redirect('/products');
    }
}
