<?php

Route::group(['middleware' => ['web']], function () {

    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('/products', 'ProductController@index');
    Route::get('/products/add-product', 'ProductController@addProduct');
    Route::get('/products/edit-product/{product}', 'ProductController@editProduct');
    Route::post('/products/edit-product/edit/{product}', 'ProductController@updateProduct');
    Route::post('/products/add-product/add', 'ProductController@store');
    Route::delete('/products/delete-product/{product}', 'ProductController@destroy');

    Route::auth();

});